//
//  VisualNotificationViewController.swift
//  iOSVisualNotification
//
//  Created by Komal on 22/11/16.
//  Copyright © 2016 Komal. All rights reserved.
//

import Foundation
import UIKit
import OneSignal

class VisualNotificationViewController: NSObject  {
    
    //MARK : Initialise Onesignal SDK with app ID which includes notification received completion block, notification action completion block and settings to be done to promt notification, etc.
    func initialiseOneSignalWithAppID(_ launchOptions: [AnyHashable : Any]! = [:], appId: String!) {
        OneSignal.initWithLaunchOptions(launchOptions, appId: appId, handleNotificationReceived:{ (notification) in
            // This block gets called when the notification is received, modify contents of notification payload
            let identifier = ProcessInfo.processInfo.globallyUniqueString
            
            //modify received title, subtitle within 30 sec before displaying to user - optional
            let content = UNMutableNotificationContent()
            let payloadAttachment = notification?.payload.attachments
            let attachmentURL = payloadAttachment?["id"]
            print("\(attachmentURL)")
            if attachmentURL != nil {
                let url = URL(string: attachmentURL as! String)
                //method called to download an image from url received in notification
                self.getDataFromUrl(url: url!) { (data, response, error)  in
                    guard let data = data, error == nil else {
                        return
                    }
                    print("Download Finished")
                    DispatchQueue.main.async() { () -> Void in
                        let attachmentImage = UIImage(data: data)
                        if let attachment = UNNotificationAttachment.create(identifier: identifier, image: attachmentImage!, options: nil) {
                            content.attachments = [attachment] //pass an attachment to notification content only supported in iOS 10
                        }
                    }
                }
            }
            
            //trigger noptification after 120.0 sec time interval so that contents are modified and displayed to user by adding request to notification center
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 120.0, repeats: false)
            let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error) in
                // handle error
            }
            
        }, handleNotificationAction:{ (result) in
            
            // This block gets called when the user reacts to a notification received
            let payload = result?.notification.payload
            var fullMessage = payload?.title
            
            //Try to fetch the action selected e.g. Share button clicked, Like button clicked
            if let actionSelected = result?.action.actionID {
                fullMessage =  fullMessage! + "\nPressed ButtonId:\(actionSelected)"
            }
            
        }, settings: [kOSSettingsKeyAutoPrompt : true, kOSSettingsKeyInFocusDisplayOption : OSNotificationDisplayType.notification.rawValue]) //settings to be done for SDK like prompting notification alert, displaying notification type.
    }
    
    //MARK: download an image data from image URL
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void)
    {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in completion (data, response, error)
            }.resume()
    }
    
}


//
//  ViewController.swift
//  iOSVisualNotification
//
//  Created by Komal on 16/11/16.
//  Copyright © 2016 Komal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var sampleWebview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let url = NSURL (string: "http://inspirationseek.com/how-to-care-for-orchids/")
        let requestObj = NSURLRequest(url: url! as URL)
        sampleWebview.loadRequest(requestObj as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


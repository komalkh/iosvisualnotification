# README: iOSVisualNotification #

iOS Visual Notification is an referance app which helps to implement push notification in iOS 10 with Onesignal SDK.

iOSVisualNotification will help developers to attach media like images in notification before dispalying it user.

iOSVisualNotification uses [OneSignal](https://github.com/OneSignal/OneSignal-iOS-SDK) for the purpose of dispalying push notification.

## For integration through cocoapods ##
Note: Cocoapods should already be installed in your mac, if not follow the steps in http://guides.cocoapods.org/using/getting-started.html

1 Open terminal and navigate to your project directory

2 Run the following command
```
pod init
```

3 After the terminal operation finishes, open the file "Podfile" and insert the following line below target & end. Save the file after. Dont forget to add use_frameworks! line in between target and end for Xcode 8.1
```
pod 'OneSignal', '~> 2.1.16'
```

4 Run the following command
```
pod install
```

5 After the terminal operation finishes, the OneSignal library files should already be in the Pods directory along with the other pod dependencies


## For manual integration ##

Initialising the library is pretty straight forward. Download the source code and add the files in "iOSVisualNotificationFolder" into your project.

## Setup the iOSVisualNotification ##

To initialise the iOSVisualNotification make the following changes in your AppDelegate. The designated initialiser takes two arguments, one is the launch option for the APIs the app will be using and the app id using which the app is registered with Onesignal SDK. 

```
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
// Override point for customization after application launch.

//Replace '5bf569d4-9aea-45be-81b9-164c8a0b153b' with your OneSignal App ID.
let visualNotificationViewController = VisualNotificationViewController()
visualNotificationViewController.initialiseOneSignalWithAppID(launchOptions, appId: "5bf569d4-9aea-45be-81b9-164c8a0b153b")

return YES;
}
```
Please refer sample app included with the library for further details related to implementation.
